import React, { useEffect, useState } from "react";

function PresentationForm() {
  const [conferences, setConferences] = useState([]);
  const [presenter_name, setPresenterName] = useState("");
  const [presenter_email, setPresenterEmail] = useState("");
  const [company_name, setCompanyName] = useState("");
  const [title, setTitle] = useState("");
  const [synopsis, setSynopsis] = useState("");
  const [conference, setConference] = useState("");

  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setConferences(data.conferences);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.presenter_name = presenter_name;
    data.presenter_email = presenter_email;
    data.company_name = company_name;
    data.title = title;
    data.synopsis = synopsis;
    data.conference = conference;

    const conferenceId = document.getElementById("conference").value;
    const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;

    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(presentationUrl, fetchConfig);

    if (response.ok) {
      const newPresentation = await response.json();
      console.log(newPresentation);
      setPresenterName("");
      setPresenterEmail("");
      setCompanyName("");
      setTitle("");
      setSynopsis("");
      setConference("");
    }
  };

  const handlePresenterNameChange = (event) => {
    setPresenterName(event.target.value);
  };

  const handlePresenterEmailChange = (event) => {
    setPresenterEmail(event.target.value);
  };

  const handleCompanyNameChange = (event) => {
    setCompanyName(event.target.value);
  };

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  const handleSynopsisChange = (event) => {
    setSynopsis(event.target.value);
  };

  const handleConferenceChange = (event) => {
    setConference(event.target.value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new presentation</h1>
          <form id="create-presentation-form" onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input
                onChange={handlePresenterNameChange}
                placeholder="Presentation name"
                required
                type="text"
                name="presenter_name"
                id="presenter_name"
                className="form-control"
                value={presenter_name}
              />
              <label htmlFor="name">Presentation name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handlePresenterEmailChange}
                placeholder="Presenter email"
                required
                type="email"
                name="presenter_email"
                id="presenter_email"
                className="form-control"
                value={presenter_email}
              />
              <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleCompanyNameChange}
                placeholder="Company name"
                type="text"
                name="company_name"
                id="company_name"
                className="form-control"
                value={company_name}
              />
              <label htmlFor="company_name">Company name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleTitleChange}
                placeholder="Title"
                required
                type="text"
                name="title"
                id="title"
                className="form-control"
                value={title}
              />
              <label htmlFor="title">Title</label>
            </div>
            <div className="mb-3">
              <label htmlFor="synopsis" className="synopsis">
                Synopsis
              </label>
              <textarea
                onChange={handleSynopsisChange}
                className="form-control"
                name="synopsis"
                id="synopsis"
                rows="3"
                value={synopsis}
              ></textarea>
            </div>
            <div className="mb-3">
              <select
                onChange={handleConferenceChange}
                required
                name="conference"
                id="conference"
                className="form-select"
                value={conference}
              >
                <option value="">Choose a conference</option>
                {conferences.map((conference) => (
                  <option key={conference.id} value={conference.id}>
                    {conference.name}
                  </option>
                ))}
              </select>
            </div>
            <div>
              <button className="btn btn-primary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default PresentationForm;
