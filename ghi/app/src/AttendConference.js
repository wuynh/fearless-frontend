import React, { useEffect, useState } from "react";

function AttendConference() {
  const [conferences, setConferences] = useState([]);
  const [conference, setConference] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [successMessage, setSuccessMessage] = useState(false);

  const fetchData = async () => {
    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const conferenceResponse = await fetch(conferenceUrl);

    if (conferenceResponse.ok) {
      const conferenceData = await conferenceResponse.json();
      setConferences(conferenceData.conferences);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.conference = conference;
    data.name = name;
    data.email = email;

    const attendeeUrl = "http://localhost:8001/api/attendees/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(attendeeUrl, fetchConfig);
    if (response.ok) {
      const newAttendee = await response.json();
      setSuccessMessage(true);
      setConference("");
      setName("");
      setEmail("");
    } else {
        const errorData = await response.json();
        console.log(errorData);
        }
  };

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    let spinner = document.getElementById("loading-conference-spinner");
    let select = document.getElementById("conference");
    if (conferences.length > 0) {
      spinner.classList.add("d-none");
      select.classList.remove("d-none");
    }
  }, [conferences]);

  useEffect(() => {
    let successMessageElement = document.getElementById("success-message");
    let formElement = document.getElementById("create-attendee-form");
    if (successMessage) {
      formElement.classList.add("d-none");
      successMessageElement.classList.remove("d-none");
    } else {
      formElement.classList.remove("d-none");
      successMessageElement.classList.add("d-none");
    }
  }, [successMessage]);

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
          <img
            width="300"
            className="bg-white rounded shadow d-block mx-auto mb-4"
            src="/logo.svg"
          />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form id="create-attendee-form" onSubmit={handleSubmit}>
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                  Please choose which conference you'd like to attend.
                </p>
                <div
                  className="d-flex justify-content-center mb-3"
                  id="loading-conference-spinner"
                >
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select
                    onChange={handleConferenceChange}
                    required
                    name="conference"
                    id="conference"
                    className="form-select d-none"
                    value={conference}
                  >
                    <option value="">Choose a conference</option>
                    {conferences.map((conference) => (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    ))}
                  </select>
                </div>
                <p className="mb-3">Now, tell us about yourself.</p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleNameChange}
                        required
                        placeholder="Your full name"
                        type="text"
                        id="name"
                        name="name"
                        className="form-control"
                        value={name}
                      />
                      <label htmlFor="name">Your full name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input
                        onChange={handleEmailChange}
                        required
                        placeholder="Your email address"
                        type="email"
                        id="email"
                        name="email"
                        className="form-control"
                        value={email}
                      />
                      <label htmlFor="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div
                className="alert alert-success d-none mb-0"
                id="success-message"
              >
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AttendConference;
